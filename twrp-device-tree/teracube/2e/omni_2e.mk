#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from 2e device
$(call inherit-product, device/teracube/2e/device.mk)

PRODUCT_DEVICE := 2e
PRODUCT_NAME := omni_2e
PRODUCT_BRAND := teracube
PRODUCT_MODEL := Teracube 2e
PRODUCT_MANUFACTURER := teracube

PRODUCT_GMS_CLIENTID_BASE := android-teracube

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="full_zirconia-user 12 SP1A.210812.016 p1rck61v164bspP6 release-keys"

BUILD_FINGERPRINT := Teracube/full_zirconia/zirconia:12/SP1A.210812.016/p1rck61v164bspP6:user/release-keys
